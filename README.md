![Alt text](https://bitbucket.org/repo/BkBnjbE/images/1237023339-icon.png)

# Rick And Morty Progressive Web App

## Demo

[![Netlify Status](https://api.netlify.com/api/v1/badges/64982e98-dca5-4844-8acc-fb6366a78190/deploy-status)](https://app.netlify.com/sites/rick-and-morty-app/deploys)

https://rick-and-morty-app.netlify.com/

## Usage

### Prerequisites

Make sure that you have Node and NPM installed.

### Installing

1- Clone this repository using

```
git clone https://semcelik@bitbucket.org/semcelik/rick-and-morty-app.git
```

2- Go to project folder with

```
cd rick-and-morty-app
```

3- Install modules with

```
npm install
```

4- Start project with

```
npm start
```

and it will run at `http://localhost:3000`
