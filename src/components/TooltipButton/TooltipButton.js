import React from 'react';
import PropTypes from 'prop-types';
import { Button, Tooltip } from 'antd';

function TooltipButton({ title, placement = 'top', children, ...rest }) {
  return (
    <Tooltip title={title} placement={placement}>
      <Button {...rest}>{children}</Button>
    </Tooltip>
  );
}

TooltipButton.propTypes = {
  title: PropTypes.string,
  placement: PropTypes.string,
};

export default TooltipButton;
