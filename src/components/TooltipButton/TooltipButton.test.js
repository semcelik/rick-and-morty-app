import React from 'react';
import { shallow } from 'enzyme';
import TooltipButton from '.';

it('should render correctly with no props', () => {
  const component = shallow(<TooltipButton />);
  expect(component).toMatchSnapshot();
});

it('should render correctly with props', () => {
  const props = {
    title: 'Test title',
    placement: 'right',
    onClick: () => {},
  };
  const buttonName = 'TestButton';

  const component = shallow(
    <TooltipButton {...props}>{buttonName}</TooltipButton>
  );

  const tooltipComponent = component.get(0);
  expect(tooltipComponent.props.title).toEqual(props.title);
  expect(tooltipComponent.props.placement).toEqual(props.placement);

  const buttonComponent = tooltipComponent.props.children;
  expect(buttonComponent.props.children).toEqual(buttonName);
  expect(buttonComponent.props.onClick).toEqual(props.onClick);
});
