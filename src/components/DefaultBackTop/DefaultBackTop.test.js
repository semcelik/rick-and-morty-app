import React from 'react';
import { shallow } from 'enzyme';
import DefaultBackTop from '.';

it('should render correctly with no props', () => {
  const component = shallow(<DefaultBackTop />);
  expect(component).toMatchSnapshot();
});
