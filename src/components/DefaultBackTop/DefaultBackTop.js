import './DefaultBackTop.scss';

import React from 'react';
import { BackTop } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowUp } from '@fortawesome/free-solid-svg-icons';

function DefaultBackTop(props) {
  return (
    <BackTop {...props}>
      <div className="default-back-top">
        <FontAwesomeIcon icon={faArrowUp} />
      </div>
    </BackTop>
  );
}

DefaultBackTop.propTypes = {};

export default DefaultBackTop;
