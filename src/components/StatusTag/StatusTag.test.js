import React from 'react';
import { shallow } from 'enzyme';
import StatusTag from '.';
import { STATUS_TYPE } from '../../constants/dictionaries';

it('should render correctly with no props', () => {
  const component = shallow(<StatusTag />);
  expect(component).toMatchSnapshot();
});

it('should render correctly with props', () => {
  const type = STATUS_TYPE.ALIVE;
  const component = shallow(<StatusTag type={type} />);
  expect(component.get(0).props.children[0]).toEqual(type);
});
