import './StatusTag.scss';

import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faUserCircle,
  faSkullCrossbones,
  faQuestionCircle,
} from '@fortawesome/free-solid-svg-icons';
import { STATUS_TYPE } from '../../constants/dictionaries';
import colors from '../../constants/colors';

const ICON = {
  ALIVE: { icon: faUserCircle, color: colors.primaryGreen },
  DEAD: { icon: faSkullCrossbones, color: colors.primaryRed },
  UNKNOWN: { icon: faQuestionCircle, color: colors.unknownIconColor },
};

function StatusTag({ type }) {
  const { icon, color } = useTagInfo(type);
  return (
    <div className="status-tag" style={{ backgroundColor: color }}>
      {type}
      <FontAwesomeIcon icon={icon} />
    </div>
  );
}

function useTagInfo(type) {
  const [tagInfo, setTagInfo] = useState(ICON.UNKNOWN);

  useEffect(() => {
    let tagInfoToSet = ICON.UNKNOWN;
    if (type === STATUS_TYPE.ALIVE) {
      tagInfoToSet = ICON.ALIVE;
    } else if (type === STATUS_TYPE.DEAD) {
      tagInfoToSet = ICON.DEAD;
    }
    setTagInfo(tagInfoToSet);
  }, [type]);

  return tagInfo;
}

StatusTag.propTypes = {
  type: PropTypes.string,
};

export default StatusTag;
