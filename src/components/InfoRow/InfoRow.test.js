import React from 'react';
import { shallow, mount } from 'enzyme';
import InfoRow from '.';

it('should render correctly with no props', () => {
  const component = shallow(<InfoRow />);
  expect(component).toMatchSnapshot();
});

it('should render correctly with props', () => {
  const testValues = ['test label', 'test value'];
  const component = shallow(
    <InfoRow label={testValues[0]} value={testValues[1]} />
  );

  component.children().map((childCol, i) => {
    expect(childCol.exists()).toEqual(true);
    expect(childCol.children().text()).toEqual(testValues[i]);
  });
});
