import './InfoRow.scss';

import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'antd';

const SPAN = {
  LABEL: 8,
  VALUE: 16,
};

function InfoRow({
  label,
  labelSpan = SPAN.LABEL,
  value,
  valueSpan = SPAN.VALUE,
}) {
  return (
    <Row className="info-row">
      <Col className="label" span={labelSpan}>
        {label}
      </Col>
      <Col span={valueSpan}>{value}</Col>
    </Row>
  );
}

InfoRow.propTypes = {
  label: PropTypes.node,
  labelSpan: PropTypes.number,
  value: PropTypes.node,
  valueSpan: PropTypes.number,
};

export default InfoRow;
