import CharacterListPage from './pages/CharacterListPage';
import CharacterDetailPage from './pages/CharacterDetailPage';
import NotFoundPage from './pages/NotFoundPage';

const ROUTES = [
  {
    key: 'list',
    exact: true,
    path: '/',
    component: CharacterListPage,
  },
  {
    key: 'detail',
    exact: true,
    path: '/detail/:id',
    component: CharacterDetailPage,
  },
  {
    key: 'not-found',
    component: NotFoundPage,
  },
];

export default ROUTES;
