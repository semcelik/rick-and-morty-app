import './NotFoundPage.scss';

import React from 'react';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import mortyFace from './../../assets/images/morty-face.png';

function NotFoundPage() {
  return (
    <div className="not-found-page">
      <Helmet>
        <title>OOH JEEZ</title>
      </Helmet>
      <div>
        <span className="message-title">OOH JEEZ,</span>
        <div className="error-row">
          <span className="error-letter">4</span>
          <img alt="morty-face" className="morty-face" src={mortyFace} />
          <span className="error-letter">4</span>
        </div>
        <span>PAGE NOT FOUND</span>
      </div>
      <div className="home-redirect">
        <Link to="/">click</Link> to go home
      </div>
    </div>
  );
}

NotFoundPage.propTypes = {};

export default NotFoundPage;
