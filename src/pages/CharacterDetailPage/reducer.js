import types from './action-types';

const initialState = {
  characterDetail: {},
  detailedEpisodesLoading: false,
  detailedEpisodes: [],
};

const characterDetailPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.CHARACTER_DETAIL_LOADED:
      return {
        ...state,
        characterDetail: action.characterDetail,
      };
    case types.LOAD_DETAILED_EPISODES:
      return {
        ...state,
        detailedEpisodesLoading: true,
      };
    case types.DETAILED_EPISODES_LOADED:
      return {
        ...state,
        detailedEpisodes: action.detailedEpisodes,
        detailedEpisodesLoading: false,
      };
    case types.RESET_REDUCER:
      return { ...initialState };
    default:
      return state;
  }
};

export default characterDetailPageReducer;
