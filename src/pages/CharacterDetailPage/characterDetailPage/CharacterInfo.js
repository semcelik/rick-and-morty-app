import React from 'react';
import PropTypes from 'prop-types';
import StatusTag from './../../../components/StatusTag';
import InfoRow from '../../../components/InfoRow';

const CustomInfoRow = (props) => (
  <InfoRow labelSpan={12} valueSpan={12} {...props} />
);

function CharacterInfo({
  image,
  status,
  species,
  origin = {},
  lastLocation = {},
}) {
  return (
    <div className="character-info">
      <div className="info-image" style={{ backgroundImage: `url(${image})` }}>
        <StatusTag type={status} />
      </div>
      <CustomInfoRow label="Species" value={species} />
      <CustomInfoRow label="Origin" value={origin.name} />
      <CustomInfoRow label="Last Location" value={lastLocation.name} />
    </div>
  );
}

CharacterInfo.propTypes = {
  image: PropTypes.string,
  status: PropTypes.string,
  species: PropTypes.string,
  origin: PropTypes.object,
  lastLocation: PropTypes.object,
};

export default CharacterInfo;
