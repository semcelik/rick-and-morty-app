import React from 'react';
import PropTypes from 'prop-types';
import HomeButton from '../../../containers/HomeButton';

function CharacterDetailHeader({ name, gender }) {
  return (
    <div className="character-detail-header">
      <HomeButton className="back-button" />
      <div className="info-wrapper">
        <span className="name">{name}</span>
        <span className="gender">{gender}</span>
      </div>
    </div>
  );
}

CharacterDetailHeader.propTypes = {
  name: PropTypes.string,
  gender: PropTypes.string,
};

export default CharacterDetailHeader;
