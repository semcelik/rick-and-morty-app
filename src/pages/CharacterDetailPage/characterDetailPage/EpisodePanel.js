import React from 'react';
import EpisodeList from './episodePanel/EpisodeList';

function EpisodePanel() {
  return (
    <div className="episode-panel-wrapper">
      <div className="episode-panel">
        <div className="episode-panel-header">Episodes</div>
        <EpisodeList />
      </div>
    </div>
  );
}

EpisodePanel.propTypes = {};

export default EpisodePanel;
