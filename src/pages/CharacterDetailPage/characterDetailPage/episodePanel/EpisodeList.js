import React, { useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { List } from 'antd';
import { sortBy } from 'lodash';
import { loadDetailedEpisodes } from './../../actions';
import { getEpisodeNumber } from './../../../../helpers/api-helper';
import { CharacterDetailPageContext } from '../../CharacterDetailPage';
import EpisodeListItem from './episodeList/EpisodeListItem';

const SIZE_TO_SLICE = 5;

function EpisodeList({
  detailedEpisodes,
  detailedEpisodesLoading,
  loadDetailedEpisodes,
}) {
  const { episodes } = useContext(CharacterDetailPageContext);

  useEffect(() => {
    const lastFiveEpisodeIds = sortBy(episodes, (e) => -getEpisodeNumber(e))
      .slice(0, SIZE_TO_SLICE)
      .map(getEpisodeNumber);
    if (lastFiveEpisodeIds.length) {
      loadDetailedEpisodes(lastFiveEpisodeIds);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [episodes]);

  return (
    <List
      className="episode-list"
      loading={detailedEpisodesLoading}
      dataSource={detailedEpisodes}
      renderItem={({ id, episode, name, air_date: airDate }) => (
        <EpisodeListItem episode={episode} name={name} airDate={airDate} />
      )}
    />
  );
}

EpisodeList.propTypes = {
  detailedEpisodes: PropTypes.array,
  detailedEpisodesLoading: PropTypes.bool,
  loadDetailedEpisodes: PropTypes.func,
};

const mapStateToProps = ({
  characterDetailPageReducer: { detailedEpisodes, detailedEpisodesLoading },
}) => ({
  detailedEpisodesLoading,
  detailedEpisodes,
});

const mapDispatchToProps = {
  loadDetailedEpisodes,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EpisodeList);
