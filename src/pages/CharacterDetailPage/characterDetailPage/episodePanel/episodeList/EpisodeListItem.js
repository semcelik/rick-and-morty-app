import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { List, Skeleton } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTv } from '@fortawesome/free-solid-svg-icons';
import { convertToKebabCase } from '../../../../../helpers/string-helper';
import { ADULT_SWIM_ENDPOINT } from '../../../../../constants/endpoints';
import TooltipButton from '../../../../../components/TooltipButton';

const { Item } = List;
const { Meta } = Item;

function EpisodeListItem({ episode, name, airDate, detailedEpisodesLoading }) {
  return (
    <Item>
      <Skeleton loading={detailedEpisodesLoading} active>
        <Meta title={episode} description={name} />
        <div className="item-content">
          <span>{airDate}</span>
          <a
            target="_blank"
            rel="noopener noreferrer"
            href={`${ADULT_SWIM_ENDPOINT}${convertToKebabCase(name)}`}
          >
            <TooltipButton
              title="Click to Watch Episode"
              placement="left"
              shape="circle"
              className="watch-btn secondary"
            >
              <FontAwesomeIcon icon={faTv} />
            </TooltipButton>
          </a>
        </div>
      </Skeleton>
    </Item>
  );
}

EpisodeListItem.propTypes = {
  episode: PropTypes.string,
  name: PropTypes.string,
  airDate: PropTypes.string,
  detailedEpisodesLoading: PropTypes.bool,
};

const mapStateToProps = ({
  characterDetailPageReducer: { detailedEpisodesLoading },
}) => ({
  detailedEpisodesLoading,
});

export default memo(connect(mapStateToProps)(EpisodeListItem));
