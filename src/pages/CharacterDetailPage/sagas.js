import { put, call, takeLatest } from 'redux-saga/effects';
import { message } from 'antd';
import types from './action-types';
import { characterDetailLoaded, detailedEpisodesLoaded } from './actions';
import api from '../../api';

function* loadCharacterDetail({ id, url, callback }) {
  const finalUrl = url || `/character/${id}`;
  const [res, error] = yield call(api.get(finalUrl, { isFullUrl: !!url }));
  if (error) {
    message.error(error.error);
  }
  if (res) {
    yield put(characterDetailLoaded(res));
  }
  callback(!!res);
}

function* loadDetailedEpisodes({ episodeIds }) {
  const [res = []] = yield call(api.get(`/episode/[${episodeIds.join()}]`));
  yield put(detailedEpisodesLoaded(res));
}

export default function* characterDetailPageSaga() {
  yield takeLatest(types.LOAD_CHARACTER_DETAIL, loadCharacterDetail);
  yield takeLatest(types.LOAD_DETAILED_EPISODES, loadDetailedEpisodes);
}
