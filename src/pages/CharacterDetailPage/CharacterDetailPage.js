import './CharacterDetailPage.scss';

import React, { createContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { loadCharacterDetail, resetReducer } from './actions';
import CharacterDetailHeader from './characterDetailPage/CharacterDetailHeader';
import CharacterInfo from './characterDetailPage/CharacterInfo';
import EpisodePanel from './characterDetailPage/EpisodePanel';

function CharacterDetailPage({
  location,
  match,
  history,
  characterDetail,
  loadCharacterDetail,
  resetReducer,
}) {
  useEffect(() => {
    const { state: { url } = {} } = location;
    const {
      params: { id },
    } = match;
    loadCharacterDetail({ url, id }, (isSuccessful) => {
      if (!isSuccessful) {
        history.push('/not-found');
      }
    });
    return () => {
      resetReducer();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const {
    image,
    name,
    gender,
    status,
    species,
    origin,
    location: lastLocation,
    episode: episodes,
  } = characterDetail;

  return (
    <CharacterDetailPageContext.Provider value={{ episodes }}>
      <div className="character-detail-page">
        <Helmet>
          <title>{name}</title>
        </Helmet>
        <CharacterDetailHeader name={name} gender={gender} />
        <CharacterInfo
          image={image}
          status={status}
          species={species}
          origin={origin}
          lastLocation={lastLocation}
        />
        <EpisodePanel />
      </div>
    </CharacterDetailPageContext.Provider>
  );
}

CharacterDetailPage.propTypes = {
  location: PropTypes.object,
  match: PropTypes.object,
  characterDetail: PropTypes.object,
  loadCharacterDetail: PropTypes.func,
  resetReducer: PropTypes.func,
};

const mapStateToProps = ({ characterDetailPageReducer }) => ({
  characterDetail: characterDetailPageReducer.characterDetail,
});

const mapDispatchToProps = {
  loadCharacterDetail,
  resetReducer,
};

export const CharacterDetailPageContext = createContext();

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CharacterDetailPage);
