import types from './action-types';

export const loadCharacterDetail = ({ id, url }, callback) => ({
  type: types.LOAD_CHARACTER_DETAIL,
  id,
  url,
  callback,
});

export const characterDetailLoaded = (characterDetail) => ({
  type: types.CHARACTER_DETAIL_LOADED,
  characterDetail,
});

export const loadDetailedEpisodes = (episodeIds) => ({
  type: types.LOAD_DETAILED_EPISODES,
  episodeIds,
});

export const detailedEpisodesLoaded = (detailedEpisodes) => ({
  type: types.DETAILED_EPISODES_LOADED,
  detailedEpisodes,
});

export const resetReducer = () => ({ type: types.RESET_REDUCER });
