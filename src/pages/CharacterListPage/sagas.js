import { put, call, select, takeLatest } from 'redux-saga/effects';
import types from './action-types';
import { charactersLoaded, loadCharactersError } from './actions';
import api from '../../api';

const getNext = (state) => state.characterListPageReducer.next;

function* loadCharacters() {
  const next = yield select(getNext);
  const finalUrl = next || '/character';
  const [res] = yield call(api.get(finalUrl, { isFullUrl: !!next }));
  if (res) {
    const {
      info: { next },
      results,
    } = res;
    yield put(charactersLoaded(results, next));
  } else {
    yield put(loadCharactersError());
  }
}

export default function* characterListPageSaga() {
  yield takeLatest(types.LOAD_CHARACTERS, loadCharacters);
}
