import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import moment from 'moment';
import InfoRow from '../../../components/InfoRow';

function CharacterCard({
  id,
  name,
  url,
  image,
  created,
  species,
  origin = {},
}) {
  return (
    <div className="character-card">
      <Link
        to={{
          pathname: `/detail/${id}`,
          state: { url },
        }}
      >
        <span className="id">{id}</span>
        <div
          className="background"
          style={{ backgroundImage: `url(${image})` }}
        />
        <div className="info">
          <InfoRow label="Name" value={name} />
          <InfoRow label="Species" value={species} />
          <InfoRow label="Created" value={moment(created).fromNow()} />
          <InfoRow label="Origin" value={origin.name} />
        </div>
      </Link>
    </div>
  );
}

CharacterCard.propTypes = {
  id: PropTypes.number,
  name: PropTypes.string,
  url: PropTypes.string,
  image: PropTypes.string,
  created: PropTypes.string,
  species: PropTypes.string,
  origin: PropTypes.object,
};

export default memo(CharacterCard);
