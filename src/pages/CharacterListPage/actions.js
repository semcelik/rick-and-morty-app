import types from './action-types';

export const loadCharacters = () => ({ type: types.LOAD_CHARACTERS });

export const charactersLoaded = (characters, next) => ({
  type: types.CHARACTERS_LOADED,
  characters,
  next,
});

export const loadCharactersError = () => ({
  type: types.LOAD_CHARACTERS_ERROR,
});

export const resetReducer = () => ({ type: types.RESET_REDUCER });
