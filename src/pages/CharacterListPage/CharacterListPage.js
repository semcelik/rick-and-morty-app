import './CharacterListPage.scss';

import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { Col, Row } from 'antd';
import InfiniteScroll from 'react-infinite-scroller';
import { loadCharacters, resetReducer } from './actions';
import DefaultBackTop from '../../components/DefaultBackTop';
import CharacterCard from './characterListPage/CharacterCard';

function CharacterListPage({
  isFinished,
  characters,
  charactersLoading,
  loadCharacters,
  resetReducer,
}) {
  const pageRef = useRef();

  useEffect(() => {
    return () => {
      resetReducer();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div ref={pageRef} className="character-list-page-wrapper">
      <Helmet>
        <title>Rick And Morty App</title>
      </Helmet>
      <InfiniteScroll
        pageStart={0}
        loadMore={loadCharacters}
        hasMore={!charactersLoading && !isFinished}
        useWindow={false}
      >
        <Row type="flex" justify="center">
          {characters.map(
            ({ id, name, url, image, created, species, origin }) => (
              <Col key={id}>
                <CharacterCard
                  id={id}
                  name={name}
                  url={url}
                  image={image}
                  created={created}
                  species={species}
                  origin={origin}
                />
              </Col>
            )
          )}
        </Row>
      </InfiniteScroll>
      {pageRef.current && <DefaultBackTop target={() => pageRef.current} />}
    </div>
  );
}

CharacterListPage.propTypes = {
  isFinished: PropTypes.bool,
  characters: PropTypes.array,
  charactersLoading: PropTypes.bool,
  loadCharacters: PropTypes.func,
  resetReducer: PropTypes.func,
};

const mapStateToProps = ({ characterListPageReducer }) => ({
  charactersLoading: characterListPageReducer.charactersLoading,
  characters: characterListPageReducer.characters,
  isFinished: characterListPageReducer.isFinished,
});

const mapDispatchToProps = {
  loadCharacters,
  resetReducer,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CharacterListPage);
