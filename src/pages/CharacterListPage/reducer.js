import types from './action-types';

const initialState = {
  charactersLoading: false,
  characters: [],
  next: undefined,
  isFinished: false,
};

const characterListPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.LOAD_CHARACTERS:
      return {
        ...state,
        charactersLoading: true,
      };
    case types.CHARACTERS_LOADED:
      return {
        ...state,
        characters: [...state.characters, ...action.characters],
        next: action.next,
        charactersLoading: false,
        isFinished: !action.next,
      };
    case types.LOAD_CHARACTERS_ERROR:
      return {
        ...state,
        charactersLoading: false,
      };
    case types.RESET_REDUCER:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

export default characterListPageReducer;
