import moment from 'moment';

const getFormattedDate = (dateToFormat, format = 'MMM Do YY') =>
  moment(dateToFormat).format(format);

export { getFormattedDate };
