const convertToKebabCase = (stringValue) =>
  stringValue
    .match(/[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g)
    .map((value) => value.toLowerCase())
    .join('-');

const joinNonNil = (array, delimiter = ' ') =>
  array.filter((item) => item != null).join(delimiter);

export { convertToKebabCase, joinNonNil };
