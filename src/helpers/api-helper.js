import { API_ENDPOINT } from './../constants/endpoints';

const EPISODE_API_PREFIX = `${API_ENDPOINT}episode/`;

const getEpisodeNumber = (episode) =>
  Number(episode.replace(EPISODE_API_PREFIX, ''));

export { getEpisodeNumber };
