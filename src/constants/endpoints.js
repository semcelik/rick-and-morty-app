const API_ENDPOINT = 'https://rickandmortyapi.com/api/';

const ADULT_SWIM_ENDPOINT = 'https://www.adultswim.com/videos/rick-and-morty/';

export { API_ENDPOINT, ADULT_SWIM_ENDPOINT };
