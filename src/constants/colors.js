const colors = {
  primaryGreen: '#00a827',
  primaryRed: '#e53935',
  unknownIconColor: '#616161',
};

export default colors;
