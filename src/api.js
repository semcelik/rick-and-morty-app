import axios from 'axios';
import qs from 'qs';
import { API_ENDPOINT } from './constants/endpoints';

const parseResponse = ({ data }) => [data];

const onError = ({ response: { data } }) => [undefined, data];

function instance(
  method,
  baseUrl = '',
  url,
  { params, isFullUrl = false, ...rest } = {}
) {
  const finalUrl = isFullUrl
    ? url
    : `${baseUrl}${url.startsWith('/') ? url.slice(1, url.length) : url}`;
  return axios({
    method,
    params,
    url: finalUrl,
    paramsSerializer: (queryParams) =>
      qs.stringify(queryParams, { arrayFormat: 'repeat' }),
    ...rest,
  });
}

function baseRequest(baseUrl) {
  return {
    get: (url, props) => () =>
      instance('get', baseUrl, url, props)
        .then(parseResponse)
        .catch(onError),
  };
}

const api = baseRequest(API_ENDPOINT);

export default api;
