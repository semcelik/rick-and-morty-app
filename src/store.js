import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import characterListPageReducer from './pages/CharacterListPage/reducer';
import characterDetailPageReducer from './pages/CharacterDetailPage/reducer';
import characterListPageSaga from './pages/CharacterListPage/sagas';
import characterDetailPageSaga from './pages/CharacterDetailPage/sagas';
import characterSearchContainerSaga from './containers/CharacterSearch/sagas';

const sagaMiddleware = createSagaMiddleware();

const combinedReducers = combineReducers({
  characterListPageReducer,
  characterDetailPageReducer,
});

const enhancers = [applyMiddleware(sagaMiddleware)];

const composeEnhancers =
  process.env.NODE_ENV !== 'production' &&
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : compose;

const store = createStore(combinedReducers, composeEnhancers(...enhancers));

const sagas = [
  characterListPageSaga,
  characterDetailPageSaga,
  characterSearchContainerSaga,
];

sagas.forEach((saga) => {
  sagaMiddleware.run(saga);
});

export default store;
