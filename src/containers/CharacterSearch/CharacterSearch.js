import './CharacterSearch.scss';

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Select } from 'antd';
import { loadCharactersByName } from './actions';
import { joinNonNil } from './../../helpers/string-helper';

const { Option } = Select;

const PLACEHOLDER = 'Search by name';

function CharacterSearch({
  size = 'large',
  className,
  loadCharactersByName,
  history,
  ...rest
}) {
  const [charactersLoading, setCharactersLoading] = useState(false);
  const [characters, setCharacters] = useState([]);

  const handleSearch = (value) => {
    if (value) {
      setCharactersLoading(true);
      loadCharactersByName(value, (characters) => {
        setCharacters(characters);
        setCharactersLoading(false);
      });
    }
  };

  const handleSelect = (value) => {
    history.push(`detail/${value}`);
  };

  const { location, match, ...selectProps } = rest;
  return (
    <Select
      {...selectProps}
      showSearch
      size={size}
      className={joinNonNil(['character-search', className])}
      loading={charactersLoading}
      placeholder={PLACEHOLDER}
      defaultActiveFirstOption={false}
      filterOption={false}
      onSearch={handleSearch}
      onSelect={handleSelect}
    >
      {characters.map(({ id, name }) => (
        <Option key={id} value={id}>
          {name}
        </Option>
      ))}
    </Select>
  );
}

CharacterSearch.propTypes = {
  size: PropTypes.string,
  className: PropTypes.string,
  loadCharactersByName: PropTypes.func,
  history: PropTypes.object,
  location: PropTypes.object,
  match: PropTypes.object,
};

const mapDispatchToProps = {
  loadCharactersByName,
};

export default withRouter(
  connect(
    null,
    mapDispatchToProps
  )(CharacterSearch)
);
