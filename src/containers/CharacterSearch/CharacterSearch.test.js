import React from 'react';
import { shallow } from 'enzyme';
import CharacterSearch from '.';

it('should render correctly with no props', () => {
  const component = shallow(<CharacterSearch />);
  expect(component).toMatchSnapshot();
});
