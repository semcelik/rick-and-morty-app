import types from './action-types';

export const loadCharactersByName = (name, callback) => ({
  type: types.LOAD_CHARACTERS_BY_NAME,
  name,
  callback,
});
