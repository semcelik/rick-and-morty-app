import { call, takeLatest, delay } from 'redux-saga/effects';
import types from './action-types';
import api from '../../api';

const DELAY_FOR_SEARCH = 400;

function* loadCharactersByName({ name, callback }) {
  const params = { name };
  yield delay(DELAY_FOR_SEARCH);
  const [{ results = [] } = {}] = yield call(api.get('/character', { params }));
  callback(results);
}

export default function* characterSearchContainerSaga() {
  yield takeLatest(types.LOAD_CHARACTERS_BY_NAME, loadCharactersByName);
}
