const PREFIX = 'CHARACTER_SEARCH';

const types = {
  LOAD_CHARACTERS_BY_NAME: `${PREFIX}/LOAD_CHARACTERS_BY_NAME`,
};

export default types;
