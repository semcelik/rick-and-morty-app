import React from 'react';
import { shallow } from 'enzyme';
import HomeButton from '.';

it('should render correctly with no props', () => {
  const component = shallow(<HomeButton />);
  expect(component).toMatchSnapshot();
});
