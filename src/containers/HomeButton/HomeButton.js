import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Button } from 'antd';

function HomeButton({ shape = 'circle', icon = 'home', ...rest }) {
  return (
    <Link to={'/'}>
      <Button {...rest} shape={shape} icon={icon} />
    </Link>
  );
}

HomeButton.propTypes = {
  shape: PropTypes.string,
  icon: PropTypes.string,
};

export default HomeButton;
