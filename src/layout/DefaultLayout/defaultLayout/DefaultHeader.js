import React from 'react';
import { Layout } from 'antd';
import CharacterSearch from '../../../containers/CharacterSearch';

const { Header } = Layout;

function DefaultHeader() {
  return (
    <Header>
      <span className="header-title">RICK AND MORTY APP</span>
      <CharacterSearch className="header-search" />
    </Header>
  );
}

DefaultHeader.propTypes = {};

export default DefaultHeader;
