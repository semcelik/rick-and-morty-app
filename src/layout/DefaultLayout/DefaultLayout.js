import './DefaultLayout.scss';

import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import { Route, Switch } from 'react-router-dom';
import { Layout } from 'antd';
import ROUTES from '../../routes';
import DefaultHeader from './defaultLayout/DefaultHeader';

const { Content } = Layout;

function DefaultLayout({ location: { pathname } }) {
  return (
    <Layout className="default-layout">
      {pathname === '/' && <DefaultHeader />}
      <Content>
        <Switch>
          {ROUTES.map((routeProps) => (
            <Route {...routeProps} />
          ))}
        </Switch>
      </Content>
    </Layout>
  );
}

DefaultLayout.propTypes = {
  location: PropTypes.object,
};

export default withRouter(DefaultLayout);
